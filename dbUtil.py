import pymysql

import settings as s


def connect():
    try:
        conn = pymysql.connect(
            host=s.mysql_host,
            port=s.mysql_port,
            user=s.mysql_user,
            passwd=s.mysql_passwd,
            db=s.mysql_schema,
            charset='utf8'
        )
    except Exception:
        conn = None
    return conn


def searchSong(conn, title, year, company):
    cur = conn.cursor()
    query = """
SELECT 
    titlos, etos_par, cd, etos, etaireia
FROM
    tragoudi
        LEFT JOIN
    (SELECT 
        cd, title
    FROM
        (SELECT 
        *
    FROM
        singer_prod UNION SELECT 
        *
    FROM
        group_prod) AS g) AS m ON titlos = title
        LEFT JOIN
    cd_production ON cd = code_cd
    """
    if title or year or company:
        query = query + " WHERE "
    where = []
    if title:
        where.append(" titlos = %s " % conn.escape(title))
    if year:
        where.append(" etos = %s " % year)
    if company:
        where.append(" etaireia = '%s' " % conn.escape(company))
    where = "AND".join(where)
    query = query + where +' ORDER BY titlos'
    cur.execute(query)
    result = cur.fetchall()
    cur.close()
    return result


def searchArtist(conn, name, surname, yearFrom, yearTo, artType):
    cur = conn.cursor()
    if artType == 'singer':
        query = """
    SELECT
    ar_taut, onoma, epitheto, etos_gen
FROM
    kalitexnis
WHERE
    ar_taut IN (SELECT DISTINCT
            tragoudistis
        FROM
            singer_prod)
    """
    if artType == 'song_writer':
        query = """
    SELECT
    ar_taut, onoma, epitheto, etos_gen
FROM
    kalitexnis
WHERE
    ar_taut IN (SELECT DISTINCT
            stixourgos
        FROM
            tragoudi)
    """
    if artType == 'composer':
        query = """
    SELECT
    ar_taut, onoma, epitheto, etos_gen
FROM
    kalitexnis
WHERE
    ar_taut IN (SELECT DISTINCT
            sinthetis
        FROM
            tragoudi)
    """
    if name:
        query = query + " AND onoma = %s " % conn.escape(name)
    if surname:
        query = query + " AND epitheto = %s " % conn.escape(surname)
    if yearFrom:
        query = query + " AND etos_gen >= %s " % yearFrom
    if yearTo:
        query = query + " AND etos_gen <= %s " % yearTo

    cur.execute(query)
    result = cur.fetchall()
    cur.close()
    return result


def searchArtistId(conn, artistID):
    cur = conn.cursor()
    query = """
    SELECT ar_taut, onoma, epitheto, etos_gen FROM kalitexnis
    WHERE ar_taut = %s;
    """ % conn.escape(artistID)
    cur.execute(query)
    result = cur.fetchall()
    cur.close()
    return result


def artistUpdate(conn, name, surname, year, artistID):
    cur = conn.cursor()
    query = """
    UPDATE kalitexnis SET onoma = %s, epitheto = %s, etos_gen = %s
    WHERE ar_taut = %s
    """ % (conn.escape(name), conn.escape(surname), year, conn.escape(artistID))
    try:
        cur.execute(query)
        conn.commit()
        cur.close()
        return True
    except Exception:
        return False


def insertArtist(conn, artistId, name, surname, year):
    cur = conn.cursor()
    query = """
    INSERT INTO  kalitexnis VALUES(%s, %s, %s, %s)
    """ % (conn.escape(artistId), conn.escape(name), conn.escape(surname), year)
    try:
        cur.execute(query)
        conn.commit()
        cur.close()
        return True
    except Exception:
        return False


def allCds(conn):
    cur = conn.cursor()
    cur.execute("SELECT DISTINCT code_cd FROM cd_production")
    result = cur.fetchall()
    cur.close()
    return result


def allArtists(conn):
    cur = conn.cursor()
    cur.execute("SELECT DISTINCT ar_taut FROM kalitexnis")
    result = cur.fetchall()
    cur.close()
    return result


def insertSong(conn, title, year, cd, singer, composer, songWriter):
    result = True
    cur = conn.cursor()
    query = """
    INSERT INTO  tragoudi VALUES(%s, %s, %s, %s)
    """ % (conn.escape(title), conn.escape(composer), year, conn.escape(songWriter))
    try:
        cur.execute(query)
        conn.commit()
        cur.close()
    except Exception:
        pass
    cur = conn.cursor()
    query = """
    INSERT INTO  singer_prod VALUES(%s, %s, %s)
    """ % (conn.escape(cd), conn.escape(singer), conn.escape(title))
    try:
        cur.execute(query)
        conn.commit()
        cur.close()
    except Exception:
        result = False
    return result
