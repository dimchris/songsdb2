<div>
    <H1>Update Artist Information</H1>
    <form action="/search_artist_edit_status" method="POST" character-set="UTF-8">
        <table>
            <tr>
                <td><Label>Name</Label></td>
                <td><input type="text" size="50" maxlength="50" name="name" value = {{rows[0][1]}}></td>
            </tr>
            <tr>
                <td><Label>Surname</Label></td>
                <td><input type="text" size="50" maxlength="50" name="surname" value = {{rows[0][2]}}></td>
            </tr>
            <tr>
                <td><Label>Birth Year</Label></td>
                <td><input type="number" min = "1900" max = "2050" size="50" maxlength="50" name="year" value = {{rows[0][3]}}></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <button name = 'id' value={{rows[0][0]}}>Update Information</button>
                </td>
            </tr>
        </table>
        
    </form>
</div>
<div>
<a href="/"><button>HOME</button></a>
</div>