<div>
    <H1>Presentation of Songs</H1>
    <form action="/search_songs" method="POST" character-set="UTF-8">
        <table>
            <tr>
                <td><Label>Song Title</Label></td>
                <td><input type="text" size="50" maxlength="50" name="song_title"></td>
            </tr>
            <tr>
                <td><Label>Production Year</Label></td>
                <td><input type="text" size="50" maxlength="50" name="production_year"></td>
            </tr>
            <tr>
                <td><Label>Company</Label></td>
                <td><input type="text" size="50" maxlength="50" name="company"></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <button>submit</button>
                </td>
            </tr>
        </table>
    </form>
</div>
<div>
<a href="/"><button>HOME</button></a>
</div>