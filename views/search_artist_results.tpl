<div>
<H1>View Artist Result</H1>
<table>
    <tr>
        <th>National ID</th>
        <th>Name</th>
        <th>Surname</th>
        <th>Birth Year</th>
        <th>Edit?</th>
    </tr>
    %for r in rows:
    <tr>
        %for c in r:
        <td>{{c}}</td>
        %end
        <td>
            <form action="/search_artist" method="POST">
                <button name = 'id' value={{r[0]}}>Edit Me!</button>
            </form>
        </td>
    </tr>
    %end
</table>
</div>
<div>
<a href="/"><button>HOME</button></a>
</div>