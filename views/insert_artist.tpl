<div>
    <H1>Insert Artist</H1>
    <form action="/insert_artist" method="POST" character-set="UTF-8">
        <table>
            <tr>
                <td><Label>National Id</Label></td>
                <td><input type="text" size="50" maxlength="50" name="id" required></td>
            </tr>
            <tr>
                <td><Label>Name</Label></td>
                <td><input type="text" size="50" maxlength="50" name="name" required></td>
            </tr>
            <tr>
                <td><Label>Surname</Label></td>
                <td><input type="text" size="50" maxlength="50" name="surname" required></td>
            </tr>
            <tr>
                <td><Label>Birth Year</Label></td>
                <td><input type="number" min = "1900" max = "2050" size="50" maxlength="50" name="year" required></td>
            </tr>
        </table>
        <button>submit</button>
    </form>

</div>
<div>
<a href="/"><button>HOME</button></a>
</div>