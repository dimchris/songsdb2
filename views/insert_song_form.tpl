<div>
    <H1>Insert Song</H1>
    <form action="/insert_song" method="POST" character-set="UTF-8">
        <table>
            <tr>
                <td><Label>Title</Label></td>
                <td><input type="text" size="50" maxlength="50" name="song_title"></td>
            </tr>
            <tr>
                <td><Label>Production Year</Label></td>
                <td><input type="number" min = "1900" max = "2050" size="50" maxlength="50" name="production_year"></td>
            </tr>
            <tr>
                <td><Label>CD</Label></td>
                <td>
                    <select name = cd>
                        %for cd in cds:
                        <option value={{cd[0]}}>{{cd[0]}}</option>
                        %end
                    </select>
                </td>
            </tr>
            <tr>
                <td><Label>Singer</Label></td>
                <td>
                    <select name=singer>
                        %for artist in artists:
                        <option value={{artist[0]}}>{{artist[0]}}</option>
                        %end
                    </select>
                </td>
            </tr>
            <tr>
                <td><Label>Composer</Label></td>
                <td>
                    <select name = composer>
                        %for artist in artists:
                        <option value={{artist[0]}}>{{artist[0]}}</option>
                        %end
                    </select>
                </td>
            </tr>
            <tr>
                <td><Label>SongWriter</Label></td>
                <td>
                    <select name = songWriter>
                        %for artist in artists:
                        <option value={{artist[0]}}>{{artist[0]}}</option>
                        %end
                    </select>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <button>submit</button>
                </td>
            </tr>
        </table>
    </form>
</div>
<div>
<a href="/"><button>HOME</button></a>
</div>