<div>
    <H1>View Songs Results</H1>
    <table>
        <tr align="left">
            <th>Title</th>
            <th>Song Prod Year</th>
            <th>CD Code</th>
            <th>CD Prod Year</th>
            <th>Company</th>
        </tr>
        %for r in rows:
        <tr>
            %for c in r:
            <td style="min-width:120px">{{c}}</td>
            %end
        </tr>
        %end
    </table>
</div>
<div>
<a href="/"><button>HOME</button></a>
</div>