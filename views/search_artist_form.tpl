<div>
    <H1>Presentation of Artist</H1>
    <form action="/search_artist" method="POST" character-set="UTF-8">
        <table>
            <tr>
                <td><Label>Name</Label></td>
                <td><input type="text" size="50" maxlength="50" name="name"></td>
            </tr>
            <tr>
                <td><Label>Surname</Label></td>
                <td><input type="text" size="50" maxlength="50" name="surname"></td>
            </tr>
            <tr>
                <td><Label>Birth Year - From</Label></td>
                <td><input type="number" min = "1900" max = "2050" size="50" maxlength="50" name="year_from"></td>
            </tr>
            <tr>
                <td><Label>Birth Year - To</Label></td>
                <td><input type="number" min = "1900" max = "2050" size="50" maxlength="50" name="year_to"></td>
            </tr>
            <tr>
                <td><Label>Type</Label></td>
                <td>
                    <form>
                        <input type="radio" name="type" value="singer" checked> Singer<br>
                        <input type="radio" name="type" value="song_writer"> SongWriter<br>
                        <input type="radio" name="type" value="composer"> Composer
                    </form>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <button>submit</button>
                </td>
            </tr>
        </table>
    </form>
</div>
<div>
<a href="/"><button>HOME</button></a>
</div>