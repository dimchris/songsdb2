<html>
<head><title>{{title}}</title></head>
<body>
    <h1>{{title}}</h1>
    <ul>
        %for name, link in buttons:
        <li>
            <a href = {{link}}><button>{{name}}</button></a>
            %end
        </li>
    </ul>
</body>
</html>