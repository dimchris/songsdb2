from bottle import route, run, template, request, post
from settings import web_host, web_port
import dbUtil


@route('/')
def Index():
    info = {
        'title': 'Home',
        'buttons': [
            ("UPDATE & SEARCH ARTISTS", "search_artist"),
            ("SEARCH SONGS", "search_songs"),
            ("INSERT ARTIST", "insert_artist"),
            ("INSERT SONG", "insert_song")
        ]
    }
    return template('index', info)


@route('/search_artist')
def searchArtist():
    return (template('search_artist_form'))


@post('/search_artist')
def searchArtistResults():
    if request.forms.id:
        conn = dbUtil.connect()
        result = dbUtil.searchArtistId(conn, request.forms.id)
        conn.close()
        return template('search_artist_edit', rows=result)
    conn = dbUtil.connect()
    if conn:
        result = dbUtil.searchArtist(
            conn,
            request.forms.name.strip(),
            request.forms.surname.strip(),
            request.forms.year_from.strip(),
            request.forms.year_to.strip(),
            request.forms.type.strip()
        )
        conn.close()
        return template('search_artist_results', rows=result)
    return "Unable to connect to database. Check settings.py.."


@post("/search_artist_edit_status")
def searchArtistEdit():
    conn = dbUtil.connect()
    if conn:
        update = dbUtil.artistUpdate(
            conn,
            request.forms.name.strip(),
            request.forms.surname.strip(),
            request.forms.year.strip(),
            request.forms.id.strip()
        )
        conn.close()
        if update:
            return template('redirect', {'msg': "Update Successfull", 'page': 'search_artist'})
        if not update:
            return template('redirect', {'msg': "Update Unsuccessfull", 'page': 'search_artist'})
    return "Unable to connect to database. Check settings.py.."


@route('/search_songs')
def searchSongs():
    return template('search_songs_form')


@post('/search_songs')
def searchSongsResults():
    conn = dbUtil.connect()
    if conn:
        result = dbUtil.searchSong(
            conn,
            request.forms.song_title.strip(),
            request.forms.production_year.strip(),
            request.forms.company.strip()
        )
        conn.close()
        return template('search_songs_results', rows=result)
    return "Unable to connect to database. Check settings.py.."


@route('/insert_artist')
def insertArtist():
    return template('insert_artist')


@post('/insert_artist')
def insertArtistResults():
    conn = dbUtil.connect()
    if conn:
        result = dbUtil.insertArtist(
            conn,
            request.forms.id.strip(),
            request.forms.name.strip(),
            request.forms.surname.strip(),
            request.forms.year.strip()
        )
        conn.close()
        if result:
            return template('redirect', {'msg': "Update Successfull", 'page': 'insert_artist'})
        if not result:
            return template('redirect', {'msg': "Update Unsuccessfull", 'page': 'insert_artist'})
    return "Unable to connect to database. Check settings.py.."


@route('/insert_song')
def insertSong():
    conn = dbUtil.connect()
    if conn:
        info = {
            'cds': dbUtil.allCds(conn),
            'artists': dbUtil.allArtists(conn)
        }
        conn.close()
        return template('insert_song_form', info)
    return "Unable to connect to database. Check settings.py.."


@post('/insert_song')
def insertSongResult():
    conn = dbUtil.connect()
    if conn:

        result = dbUtil.insertSong(
            conn,
            request.forms.song_title,
            request.forms.production_year,
            request.forms.cd,
            request.forms.singer,
            request.forms.composer,
            request.forms.songWriter
        )
        if result:
            conn.commit()
            conn.close()
            return template('redirect', {'msg': "Update Successfull", 'page': 'insert_song'})
        if not result:
            conn.close()
            return template('redirect', {'msg': "Update Unsuccessfull", 'page': 'insert_song'})
    return "Unable to connect to database. Check settings.py.."


run(host=web_host, port=web_port, debug=True, reloader = True)
