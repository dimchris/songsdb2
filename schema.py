class songs:
    CD_PRODUCTION = 'cd_production'
    KALITEXNIS = 'kalitexnis'
    SINGER_PROD = 'singer_prod'
    TRAGOUDI = 'tragoudi'
    GROUP_PROD = 'group_prod'
    SIGROTIMA = 'sigrotima'

    class cd_production:
        CODE_CD = 'code_cd'
        ETAIRIA = 'etairia'
        ETOS = 'etos'

    class kalitexnis:
        AR_TAUT = 'ar_taut'
        ONOMA = 'onoma'
        EPITHETO = 'epitheto'
        ETOS_GEN = 'etos_gen'

    class singer_prod:
        CD = 'cd'
        TRAGOUDISTIS = 'tragoudistis'
        TITLE = 'title'

    class tragoudi:
        TITLOS = 'titlos'
        SINTHETIS = 'sinthetis'
        ETOS_PAR = 'etos_par'
        STIXOURGOS = 'stigourgos'

    class group_prod:
        CD = 'cd'
        GROUP = 'group'
        TITLE = 'title'

    class sigrotima:
        ONOMA = 'onoma'
        IM_DIMIOURGIAS = 'im_dimiourgias'
